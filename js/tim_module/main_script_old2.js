// Global Variables
let statusx // Resultado de analisis pass(1)/Fail(0) se utilica en la funcion logresult()
let logsave=[]  //Array que utilizamos en la funcion logresult() y  almacena resumen
let cuadranteArray=[] // Array para evaluar pass o fail de cuadrante
let porcentajeArray=[] // Array que guarda el porcentaje en cada punto 
let mtxw=[] //Array que guarda el porcentaje 
let barprogress=0 // Variable que se ultiliza para incrementar el valor de la barra en la funcion progressbar
let criterio // Esta variable se utiliza como criterio de pase para % de TIM x cada TA
let ruido 
let snfile,snr
let pn
let station 
let latapona 

// Canvas
let canvasjs = document.getElementById('canvas')
let context = canvasjs.getContext('2d')

let fullimage = document.getElementById( 'canvas2' )// Imagen donde se va a mapear
let contextfullimage = fullimage.getContext( '2d' )

let canvica = document.getElementById( 'canvica' )// Imagen donde se va a mapear
let contextcanvica = canvica.getContext( '2d' )

let canvande = document.getElementById( 'canvande' )// Imagen donde se va a mapear
let contextcanvande = canvande.getContext( '2d' )

let canvedio = document.getElementById( 'canvedio' )// Imagen donde se va a mapear
let contextcanvedio = canvedio.getContext( '2d' )

let canvediojr = document.getElementById( 'canvediojr' )// Imagen donde se va a mapear
let contextcanvediojr = canvediojr.getContext( '2d' )

let canhorizontal = document.getElementById( 'canhorizontal' )// Imagen donde se va a mapear camara6
let contextcanhorizontal = canhorizontal.getContext( '2d' )

let canparalelo = document.getElementById( 'canparalelo' )// Imagen donde se va a mapear camara6
let contextcanparalelo = canparalelo.getContext( '2d' )

let canvach = document.getElementById( 'canvach' )// Imagen donde se va a mapear camara7
let contextcanvach = canvach.getContext( '2d' )

let canvage = document.getElementById( 'canvage' )// Imagen donde se va a mapear camara7
let contextcanvage = canvage.getContext( '2d' )

let fiducial = document.getElementById( 'fiducial' )// Imagen de fiduciales
let contextfiducial = fiducial.getContext( '2d' )

let tb1 = document.getElementById( 'tb1' )// Imagen donde se va a mapear camara7
let contexttb1 = tb1.getContext( '2d' )

let tb2 = document.getElementById( 'tb2' )// Imagen donde se va a mapear camara7
let contexttb2 = tb2.getContext( '2d' )

let tb3 = document.getElementById( 'tb3' )// Imagen donde se va a mapear camara7
let contexttb3 = tb3.getContext( '2d' )

let tb4 = document.getElementById( 'tb4' )// Imagen donde se va a mapear camara7
let contexttb4 = tb4.getContext( '2d' )

 let image = new Image()
 image.src = '/img/tim_module/LFTM1135558-00-X_1454102-00-A_900px.png'

 let fidupic = new Image()
 fidupic.src = '/img/tim_module/fiducials/C1A.jpg'
//*************************Socket block */
 const socket = io();

socket.on('Timsequence_start',function(infoplc){//pg migrated
	
	if (infoplc!= 0) {
	
    station = infoplc.toString().substr(2,8)
    snr=infoplc.toString().substr(11,29)
    snfile =infoplc.toString().substr(25,15)
    pn = infoplc.toString().substr(41,16)
    testsequence();//Activa bandera para continuar

	console.log("Start test sequence");
   // console.log(typeof(data))
    //console.log(infoplc)
   //console.log(pn)
	}
	else{	
	console.log("Algo salio mal en el backend");
	}});

function plc_response(logsave){
    return new Promise(async resolve =>{
        porcentajeArray= 
        ""+snr+"\nCuadrante :1\n"+
        "TA1-:  "+"status :"+logsave[1]+", "+"percent -->  "+porcentajeArray[1]*100+"(%)\n"+
        "TA2-:  "+"status :"+logsave[2]+", "+"percent -->  "+porcentajeArray[2]*100+"(%)\n"+           
        "TA11-: "+"status :"+logsave[3]+", "+"percent -->  "+porcentajeArray[11]*100+"(%)\n"+
        "TA12-: "+"status :"+logsave[4]+", "+"percent -->  "+porcentajeArray[12]*100+"(%)\n"+
        "TB1-:  "+"status :"+logsave[5]+", "+"percent -->  "+porcentajeArray[23]*100+"(%)\n\n"+
                //Cuadrante 2 
                "Cuadrante :2\n"+
        "TA3-:  "+"status :"+logsave[6]+",  "+"percent -->  "+porcentajeArray[3]*100+"(%)\n"+
        "TA4-:  "+"status :"+logsave[7]+",  "+"percent -->  "+porcentajeArray[4]*100+"(%)\n"+
        "TA9-:  "+"status :"+logsave[8]+",  "+"percent -->  "+porcentajeArray[9]*100+"(%)\n"+
        "TA10-: "+"status :"+logsave[9]+",  "+"percent -->  "+porcentajeArray[10]*100+"(%)\n"+
        "TB2-:  "+"status :"+logsave[10]+",  "+"percent -->  "+porcentajeArray[24]*100+"(%)\n\n"+
                // Cuadrante3
                "Cuadrante :3\n"+
        "TA5-: "+"status :"+logsave[11]+", "+"percent -->  "+porcentajeArray[5]*100+"(%)\n"+
        "TA6-: "+"status :"+logsave[12]+", "+"percent -->  "+porcentajeArray[6]*100+"(%)\n"+
        "TA7-: "+"status :"+logsave[13]+", "+"percent -->  "+porcentajeArray[7]*100+"(%)\n"+
        "TA8-: "+"status :"+logsave[14]+", "+"percent -->  "+porcentajeArray[8]*100+"(%)\n"+
        "TB3-: "+"status :"+logsave[15]+", "+"percent -->  "+porcentajeArray[25]*100+"(%)\n\n"+
                // Cuadrante 4
                "Cuadrante :4\n"+
        "TA17-: "+"status :"+logsave[16]+", "+"percent -->  "+porcentajeArray[17]*100+"(%)\n"+
        "TA18-: "+"status :"+logsave[17]+", "+"percent -->  "+porcentajeArray[18]*100+"(%)\n\n"+
                 // Cuadrante 5
                 "Cuadrante :5\n"+
        "TA15-: "+"status :"+logsave[18]+", "+"percent -->  "+porcentajeArray[15]*100+"(%)\n"+
        "TA16-: "+"status :"+logsave[19]+", "+"percent -->  "+porcentajeArray[16]*100+"(%)\n"+
        "TA19-: "+"status :"+logsave[20]+", "+"percent -->  "+porcentajeArray[19]*100+"(%)\n"+
        "TA20-: "+"status :"+logsave[21]+", "+"percent -->  "+porcentajeArray[20]*100+"(%)\n"+
        "TB4-:  "+"status :"+logsave[22]+", "+"percent -->  "+porcentajeArray[26]*100+"(%)\n\n"+
                 // Cuadrante 6
                 "Cuadrante :6\n"+
        "TA13-: "+"status :"+logsave[23]+", "+"percent -->  "+porcentajeArray[13]*100+"(%)\n"+
        "TA14-: "+"status :"+logsave[24]+", "+"percent -->  "+porcentajeArray[14]*100+"(%)\n\n"+
                 // Cuadrante 7
                 "Cuadrante :7\n"+
        "TA21-: "+"status :"+logsave[25]+", "+"percent -->  "+porcentajeArray[21]*100+"(%)\n"+
        "TC-:   "+"status :"+logsave[26]+", "+"percent -->  "+porcentajeArray[22]*100+"(%)\n"+
        "#"


   logsave=
    ""+snr+"&TA1-"+mtxw[1]*100+"%"+","+logsave[1]+
    "&TA2-"+mtxw[2]*100+"%"+","+logsave[2]+//porcentajeArray[2]+"%"+           
    "&TA11-"+mtxw[11]*100+"%"+","+logsave[3]+//porcentajeArray[3]+"%"+
    "&TA12-"+mtxw[12]*100+"%"+","+logsave[4]+//porcentajeArray[4]+
    "&TB1-"+mtxw[23]*100+"%"+","+logsave[5]+//porcentajeArray[5]+
            //Cuadrante 2 
    "&TA3-"+mtxw[3]*100+"%"+","+logsave[6]+//porcentajeArray[6]+
    "&TA4-"+mtxw[4]*100+"%"+","+logsave[7]+//porcentajeArray[7]+
    "&TA9-"+mtxw[9]*100+"%"+","+logsave[8]+//porcentajeArray[8]+
    "&TA10-"+mtxw[10]*100+"%"+","+logsave[9]+//porcentajeArray[9]+
    "&TB2-"+mtxw[24]*100+"%"+","+logsave[10]+//porcentajeArray[10]+
            // Cuadrante3
    "&TA5-"+mtxw[5]*100+"%"+","+logsave[11]+//porcentajeArray[11]+
    "&TA6-"+mtxw[6]*100+"%"+","+logsave[12]+//porcentajeArray[12]+
    "&TA7-"+mtxw[7]*100+"%"+","+logsave[13]+//porcentajeArray[13]+
    "&TA8-"+mtxw[8]*100+"%"+","+logsave[14]+//porcentajeArray[14]+
    "&TB3-"+mtxw[25]*100+"%"+","+logsave[15]+//porcentajeArray[15]+

    "&TA17-"+mtxw[17]*100+"%"+","+logsave[16]+//porcentajeArray[16]+
    "&TA18-"+mtxw[18]*100+"%"+","+logsave[17]+//porcentajeArray[17]+

    "&TA15-"+mtxw[15]*100+"%"+","+logsave[18]+//porcentajeArray[18]+
    "&TA16-"+mtxw[16]*100+"%"+","+logsave[19]+//porcentajeArray[19]+
    "&TA19-"+mtxw[19]*100+"%"+","+logsave[20]+//porcentajeArray[20]+
    "&TA20-"+mtxw[20]*100+"%"+","+logsave[21]+//porcentajeArray[21]+
    "&TB4-"+mtxw[26]*100+"%"+","+logsave[22]+//porcentajeArray[22]+

    "&TA13-"+mtxw[13]*100+"%"+","+logsave[23]+//porcentajeArray[23]+
    "&TA14-"+mtxw[14]*100+"%"+","+logsave[24]+//porcentajeArray[24]+
    
    "&TA21-"+mtxw[21]*100+"%"+","+logsave[25]+//porcentajeArray[25]+
    "&TC-"+mtxw[22]*100+"%"+","+logsave[26]+//porcentajeArray[26]+"%"+
    "#"

                
    //logsave=""+snr+"&TA0,1&TA1,1&TA2,1&TA3,1&TA4,1&TA5,1&TA6,1&TA7,1&TA8,1&TA9,1&TA10,1&TA11,1&TA12,1&TA13,1&TA14,1&TA15,1&TA16,1&TA17,1&TA18,1&TA19,1&TA20,1&TA21,1&TA22,1&TA23,1&TA24,1&TA25,0#"
    
    //console.log(logsave)

    /*mtxw= 
    ""+snr+"&TA1,"+mtxw[1]*100+"%"+","+logsave[1]+","+
    "&TA2,       "+mtxw[2]*100+"%"+","+logsave[2]+","+
    "&TA11,      "+mtxw[11]*100+"%"+","+logsave[2]+","+
    "&TA11,      "+mtxw[11]*100+"%"+","+logsave[2]+","+*/

    
    logsaving(snfile,porcentajeArray)
    socket.emit('plc_response',logsave)
    resolve('resolved')})
    }
//*************************Main test sequence */   
async function testsequence() {
    await serialnumber (snr)
    await partnumber (pn)
    await st(station)

    barprogress=0
    progressbar(0)
    loadimage()
    cuadranteArray=[]// Reinicia valor para retrabajar cuadrante 
    porcentajeArray=[]// Reinicia valor para retrabajar cuadrante 
    document.getElementById('statusbar').innerHTML = "Inspection in process"
    canbughide()
    for (point=1; point<8; point ++){
    
    console.log("Cuadrante :"+point)
    //whitelight_on()
    await open_cam(point)
    await captureimage()
    await recorTA(point) // recorta area a inspeccionar y analisa % de llenado
    //inspection()
    await stopcam()
    //whitelight_off()
    //blacklight_on()
   // await captureimage(point)
    //inspection()
    //blacklight_off() 
    //savepointresult(point)
    //pointstatus(point,status)
    progressbar(barprogress=barprogress+14.5)
    document.getElementById('statusbar').innerHTML = "Waiting to Start..."
    //reset()
    }// Cierra For
    //pointstatus(point,status)// Color del cuadrito debe ser variable
    //console.log ("Unit Test Sumary :"+logsave)// Imprime valores del array final
    
    //console.log ("cuadrante sumary :"+cuadranteArray)   
    await evalpf()
    await plc_response(logsave)//incluye la funcion para guardar el log txt
    
}

function serialnumber (sn){
    return new Promise(async resolve =>{
        elementsn= document.getElementById('sn')
        //console.log(serial)
        elementsn.innerHTML= "Serial: "+sn+""
    resolve('resolved')})
}

function partnumber (pn){
    return new Promise(async resolve =>{
        elementpn = document.getElementById('np')
        //console.log(model)
        elementpn.innerHTML= "Model: "+pn+""
    resolve('resolved')})
}

function st(st){
    return new Promise(async resolve =>{
        elementst= document.getElementById('st')
        //console.log(station)
        elementst.innerHTML= "Station: "+st+""
    resolve('resolved')})
}

function progressbar(percentagevalue){
    
    let bar = document.getElementById('statusbar')
    bar.style.width= ""+percentagevalue+"%"+""   // "20%" --> ""+ 20 +"%"+""

}

function loadimage(){//Funcion Carga la imagen caricatura del modelo 
    
    context.drawImage(image, 0, 0, image.width, image.height, 0, 0, context.canvas.width, context.canvas.height);
    
}


//**************************************************************************funciones de  procesamiento */
async function recorTA(point){
    return new Promise(async resolve =>{
    //let pictraining = point + "_nns"
    switch(point) {
        case 1: //
            //TA1
            //await snapshot(pictraining)
            contextcanvica.drawImage(fullimage,240,342,486,192,0,0,contextcanvica.canvas.width,contextcanvica.canvas.height); // coordenada y tamaño de recorte en el canvas 
            await analiza(canvica,1) // Realiza el analisis en el primer cuadro, el punto es el cuadrante que se va a mandar 
            logresult(1,statusx) // guarda en el array la posicion del cuadrante y su status
            contextfullimage.strokeStyle="#ffff00"
            contextfullimage.lineWidth = 2
            contextfullimage.strokeRect(240,342,486,192)//Dibuja rectangulo en canvas2
            //TA2
            contextcanvande.drawImage(fullimage,232,591,538,192,0,0,contextcanvande.canvas.width,contextcanvande.canvas.height);
            await analiza(canvande,2)
            logresult(2,statusx)// punto de TA, posicion en array de guardado y status
            contextfullimage.strokeStyle="#ffff00"
            contextfullimage.lineWidth = 2
            contextfullimage.strokeRect(232,591,538,192)
            //TA11
            contextcanvande.drawImage(fullimage,971,797,538,192,0,0,contextcanvande.canvas.width,contextcanvande.canvas.height);
            await analiza(canvande,11)
            logresult(3,statusx)
            contextfullimage.strokeStyle="#ffff00"
            contextfullimage.lineWidth = 2
            contextfullimage.strokeRect(971,797,538,192)
            //TA12
            contextcanvande.drawImage(fullimage,1075,391,538,192,0,0,contextcanvande.canvas.width,contextcanvande.canvas.height);
            await analiza(canvande,12)
            logresult(4,statusx)
            contextfullimage.strokeStyle="#ffff00"
            contextfullimage.lineWidth = 2
            contextfullimage.strokeRect(1075,391,538,192)  
            //TB1
            contexttb1.drawImage(fullimage,419,253,77,77,0,0,contexttb1.canvas.width,contexttb1.canvas.height);
            await analiza(tb1,23)
            logresult(5,statusx)
            contextfullimage.strokeStyle="#ffff00"
            contextfullimage.lineWidth = 2
            contextfullimage.strokeRect(419,253,77,77)
            await snapshot(point)
          break;
        case 2: // Diferente  medida de rectangulo por tamaño de pixel 
                // coordenadas correctas de cuadrante 2
                //colocar canvas correcto 
            //TA3 
            contextcanvediojr.drawImage(fullimage,447,260,332,131,0,0,contextcanvediojr.canvas.width,contextcanvediojr.canvas.height);
            await analiza(canvediojr,3)
            logresult(6,statusx)
            contextfullimage.strokeStyle="#ffff00"
            contextfullimage.lineWidth = 2
            contextfullimage.strokeRect(447,260,332,131)//Dibuja rectangulo en canvas2
            //TA4
            contextcanvedio.drawImage(fullimage,413,619,367,131,0,0,contextcanvedio.canvas.width,contextcanvedio.canvas.height);
            await analiza(canvedio,4)// Canvas que analisa y punto de calibracion
            logresult(7,statusx)
            contextfullimage.strokeStyle="#ffff00"
            contextfullimage.lineWidth = 2
            contextfullimage.strokeRect(413,619,367,131)//Dibuja rectangulo en canvas2*/
            //TA9 
            contextcanvedio.drawImage(fullimage,977,576,367,131,0,0,contextcanvedio.canvas.width,contextcanvedio.canvas.height);
            await analiza(canvedio,9)
            logresult(8,statusx)
            contextfullimage.strokeStyle="#ffff00"
            contextfullimage.lineWidth = 2
            contextfullimage.strokeRect(977,576,367,131)//Dibuja rectangulo en canvas2*/
            //TA10 
            contextcanvedio.drawImage(fullimage,1071,284,367,131,0,0,contextcanvedio.canvas.width,contextcanvedio.canvas.height);
            await analiza(canvedio,10)
            logresult(9,statusx)
            contextfullimage.strokeStyle="#ffff00"
            contextfullimage.lineWidth = 2
            contextfullimage.strokeRect(1071,284,367,131)//Dibuja rectangulo en canvas2*/
            //TB2
            contexttb2.drawImage(fullimage,485,198,52,52,0,0,contexttb2.canvas.width,contexttb2.canvas.height);
            await analiza(tb2,24)
            logresult(10,statusx)
            contextfullimage.strokeStyle="#ffff00"
            contextfullimage.lineWidth = 2
            contextfullimage.strokeRect(485,198,52,52)
            await snapshot(point)
          break;
        case 3: //
            //TA5
            contextcanvica.drawImage(fullimage,1160,751,486,192,0,0,contextcanvica.canvas.width,contextcanvica.canvas.height);
            await analiza(canvica,5)
            logresult(11,statusx)
            contextfullimage.strokeStyle="#ffff00"
            contextfullimage.lineWidth = 2
            contextfullimage.strokeRect(1160,751,486,192)//Dibuja rectangulo en canvas2*/
            //TA6
            contextcanvande.drawImage(fullimage,1139,244,538,192,0,0,contextcanvande.canvas.width,contextcanvande.canvas.height);
            await analiza(canvande,6)
            logresult(12,statusx)
            contextfullimage.strokeStyle="#ffff00"
            contextfullimage.lineWidth = 2
            contextfullimage.strokeRect(1139,244,538,192)//Dibuja rectangulo en canvas2*/
            //TA7
            contextcanvande.drawImage(fullimage,355,317,538,192,0,0,contextcanvande.canvas.width,contextcanvande.canvas.height);   
            await analiza(canvande,7)
            logresult(13,statusx)
            contextfullimage.strokeStyle="#ffff00"
            contextfullimage.lineWidth = 2
            contextfullimage.strokeRect(355,317,538,192)//Dibuja rectangulo en canvas2*/
            //TA8
            contextcanvande.drawImage(fullimage,251,716,538,192,0,0,contextcanvande.canvas.width,contextcanvande.canvas.height);
            await analiza(canvande,8)
            logresult(14,statusx)
            contextfullimage.strokeStyle="#ffff00"
            contextfullimage.lineWidth = 2
            contextfullimage.strokeRect(251,716,538,192)//Dibuja rectangulo en canvas2*/
            //TB3
            contexttb3.drawImage(fullimage,1512,966,77,77,0,0,contexttb3.canvas.width,contexttb3.canvas.height);
            await analiza(tb3,25)
            logresult(15,statusx)
            contextfullimage.strokeStyle="#ffff00"
            contextfullimage.lineWidth = 2
            contextfullimage.strokeRect(1512,966,77,77)
            await snapshot(point)
            break;
        case 4: 
            //TA17
            contextcanvica.drawImage(fullimage,626,616,486,192,0,0,contextcanvica.canvas.width,contextcanvica.canvas.height);
            await analiza(canvica,17)
            logresult(16,statusx)
            contextfullimage.strokeStyle="#ffff00"
            contextfullimage.lineWidth = 2
            contextfullimage.strokeRect(626,616,486,192)//Dibuja rectangulo en canvas2*/
            // TA18
            contextcanvica.drawImage(fullimage,559,344,486,192,0,0,contextcanvica.canvas.width,contextcanvica.canvas.height);
            await analiza(canvica,18)
            logresult(17,statusx)
            contextfullimage.strokeStyle="#ffff00"
            contextfullimage.lineWidth = 2
            contextfullimage.strokeRect(559,344,486,192)//Dibuja rectangulo en canvas2*/
            await snapshot(point)
            break;
        case 5: // Diferente  medida de rectangulo por tamaño de pixel
            //TA15
            contextcanhorizontal.drawImage(fullimage,789,434,408,161,0,0,contextcanhorizontal.canvas.width,contextcanhorizontal.canvas.height);
            await analiza(canhorizontal,15)
            logresult(18,statusx)
            contextfullimage.strokeStyle="#ffff00"
            contextfullimage.lineWidth = 2
            contextfullimage.strokeRect(789,434,408,161)//Dibuja rectangulo en canvas2*/
            //TA16 
            contextcanhorizontal.drawImage(fullimage,851,659,408,161,0,0,contextcanhorizontal.canvas.width,contextcanhorizontal.canvas.height);
            await analiza(canhorizontal,16)
            logresult(19,statusx)
            contextfullimage.strokeStyle="#ffff00"
            contextfullimage.lineWidth = 2
            contextfullimage.strokeRect(851,659,408,161)//Dibuja rectangulo en canvas2*/
            //TA19
            contextcanparalelo.drawImage(fullimage,390,352,161,450,0,0,contextcanparalelo.canvas.width,contextcanparalelo.canvas.height);
            await analiza(canparalelo,19)
            logresult(20,statusx)
            contextfullimage.strokeStyle="#ffff00"
            contextfullimage.lineWidth = 2
            contextfullimage.strokeRect(400,352,161,450)//Dibuja rectangulo en canvas2*/
            //TA20
            contextcanparalelo.drawImage(fullimage,177,351,161,450,0,0,contextcanparalelo.canvas.width,contextcanparalelo.canvas.height);
            await analiza(canparalelo,20)
            logresult(21,statusx)
            contextfullimage.strokeStyle="#ffff00"
            contextfullimage.lineWidth = 2
            contextfullimage.strokeRect(192,351,161,450)//Dibuja rectangulo en canvas2*/
            //TB4
            contexttb4.drawImage(fullimage,608,448,64,64,0,0,contexttb4.canvas.width,contexttb4.canvas.height);
            await analiza(tb4,26)
            logresult(22,statusx)
            contextfullimage.strokeStyle="#ffff00"
            contextfullimage.lineWidth = 2
            contextfullimage.strokeRect(608,448,64,64)
            await snapshot(point)
            break;
        case 6: 
            //TA13
            contextcanvica.drawImage(fullimage,978,280,486,192,0,0,contextcanvica.canvas.width,contextcanvica.canvas.height);
            await analiza(canvica,13)
            logresult(23,statusx)
            contextfullimage.strokeStyle="#ffff00"
            contextfullimage.lineWidth = 2
            contextfullimage.strokeRect(978,280,486,192)//Dibuja rectangulo en canvas2*/
            //TA14
            contextcanvica.drawImage(fullimage,1038,546,486,192,0,0,contextcanvica.canvas.width,contextcanvica.canvas.height);
            await analiza(canvica,14)
            logresult(24,statusx)
            contextfullimage.strokeStyle="#ffff00"
            contextfullimage.lineWidth = 2
            contextfullimage.strokeRect(1038,546,486,192)//Dibuja rectangulo en canvas2*/
            await snapshot(point)
          break;
        case 7: // cuadrante 7 cuanta con 2 rectangulos para analizar y guardar en array 
            //TA21 Rectangulo Chico
            contextcanvach.drawImage(fullimage,575,308,59,454,0,0,contextcanvach.canvas.width,contextcanvach.canvas.height);
            await analiza(canvach,21)
            logresult(25,statusx)
            contextfullimage.strokeStyle="#ffff00"
            contextfullimage.lineWidth = 2
            contextfullimage.strokeRect(575,308,59,454)//Dibuja rectangulo en canvas2
            //TA22 Rectangulo Grande
            contextcanvage.drawImage(fullimage,656,227,370,650,0,0,contextcanvage.canvas.width,contextcanvage.canvas.height);
            await analiza(canvage,22)
            logresult(26,statusx) 
            contextfullimage.strokeStyle="#ffff00"
            contextfullimage.lineWidth = 2
            contextfullimage.strokeRect(656,227,370,650)//Dibuja rectangulo en canvas2
            await snapshot(point)
            break;
        default:
      }
      resolve('resolved')})
}

function analiza(canvasx, point){
    return new Promise(async resolve =>{
   // canvasx = cuadri 
   let contextcanvasx = canvasx.getContext( '2d' )
    let cdata = contextcanvasx.getImageData(0,0,canvasx.width, canvasx.height);
    //console.log(cdata.data)// ver la matriz de pixeles de la imagen 
    //console.log(cdata.data.length) // ver cantidad total de pixeles 
    let malo=0 , bueno=0
    //Valores del rojo
    let rmin
    let rmax
    //Valores del verde
    let gmin
    let gmax
    //Valores del azul
    let bmin
    let bmax
    //Calibracion del color del Tim por TA     
    // Cuadrante 1
    if (point == 1){rmin=100,rmax=205,gmin=105,gmax=240,bmin=100,bmax=215, criterio= .995,  latapona= 250} // TA1 Actualizado 
    if (point == 2){rmin=100,rmax=205,gmin=115,gmax=240,bmin=100,bmax=215, criterio= .995,  latapona= 60}  //TA2 Actualizado 
    if (point == 11){rmin=100,rmax=205,gmin=115,gmax=240,bmin=100,bmax=215, criterio= .995, latapona= 60}  // TA11 Actualizado  
    if (point == 12){rmin=115,rmax=205,gmin=115,gmax=240,bmin=100,bmax=215, criterio= .995, latapona= 700} // TA12 Actualizado
    if (point == 23){rmin=100,rmax=190,gmin=110,gmax=215,bmin=100,bmax=200, criterio= .995,  latapona= 300} //TB1
    // Cuadrante 2
    if (point == 3){rmin=90,rmax=187,gmin=90,gmax=230,bmin=70,bmax=200, criterio= .995,     latapona= 110} // TA3 Actualizado
    if (point == 4){rmin=100,rmax=195,gmin=120,gmax=235,bmin=110,bmax=210, criterio= .995,  latapona= 1100}  // TA4 Actualizado
    if (point == 9){rmin=130,rmax=200,gmin=150,gmax=240,bmin=122,bmax=210, criterio= .995,  latapona= 350}  // TA9 Actuaalizado 
    if (point == 10){rmin=130,rmax=187,gmin=150,gmax=225,bmin=122,bmax=200, criterio= .995, latapona= 650} // TA10 Actualizado 
    if (point == 24){rmin=110,rmax=215,gmin=125,gmax=240,bmin=110,bmax=215, criterio= .995, latapona= 1040}  //TB2
    // Cuadrante 3
    if (point == 5){rmin=80,rmax=200,gmin=90,gmax=215,bmin=80,bmax=200, criterio= .995,     latapona= 200}  // TA5 Actualizado
    if (point == 6){rmin=110,rmax=210,gmin=110,gmax=215,bmin=100,bmax=200, criterio= .995,  latapona= 200}  // TA6 Actualizado
    if (point == 7){rmin=100,rmax=200,gmin=110,gmax=235,bmin=100,bmax=210, criterio= .995,  latapona= 50}  // TA7 Actualizado
    if (point == 8){rmin=110,rmax=215,gmin=110,gmax=225,bmin=110,bmax=210, criterio= .995,  latapona= 100}  // TA8 Actualizado
    if (point == 25){rmin=100,rmax=190,gmin=110,gmax=215,bmin=100,bmax=190, criterio= .995,  latapona= 1050} //TB3
    // Cuadrante 4
    if (point == 17){rmin=110,rmax=185,gmin=129,gmax=215,bmin=110,bmax=195, criterio= .995,  latapona= 1600} // TA17 Actualizado 
    if (point == 18){rmin=110,rmax=200,gmin=129,gmax=215,bmin=110,bmax=200, criterio= .995,  latapona= 1200} // TA18 Actualizado
    // Cuadrante 5
    if (point == 15){rmin=110,rmax=180,gmin=129,gmax=215,bmin=110,bmax=200, criterio= .995,  latapona= 250} // TA15 Actualizado
    if (point == 16){rmin=110,rmax=185,gmin=129,gmax=225,bmin=110,bmax=210, criterio= .995,  latapona= 50} // TA16 Actualizado
    if (point == 19){rmin=100,rmax=185,gmin=105,gmax=215,bmin=100,bmax=195, criterio= .995,  latapona= 1900} // TA19 Actualizado
    if (point == 20){rmin=96,rmax=185,gmin=105,gmax=215,bmin=98,bmax=195, criterio= .995,  latapona= 6450} // TA20 Actualizado
    if (point == 26){rmin=100,rmax=200,gmin=110,gmax=230,bmin=100,bmax=210, criterio= .995,  latapona= 450} //TB4
    // Cuadrante 6
    if (point == 13){rmin=110,rmax=205,gmin=110,gmax=235,bmin=110,bmax=205, criterio= .995,  latapona= 100} // TA13  Actualizado
    if (point == 14){rmin=110,rmax=205,gmin=110,gmax=235,bmin=110,bmax=205, criterio= .995,  latapona= 20} // TA14 Actualizado
    // Cuadrante 7
    if (point == 21){rmin=95,rmax=215,gmin=100,gmax=240,bmin=90,bmax=225, criterio= .995,  latapona= 1200} // TA21 Rectangulo ch
    if (point == 22){rmin=95,rmax=215,gmin=100,gmax=225,bmin=98,bmax=245, criterio= .995,  latapona= 4500} // TA22 Actualizado




    for (let i = 0; i <cdata.data.length; i += 4) { //cdata.data.length
        // Matriz para valores 
        R = cdata.data[i + 0]
        G = cdata.data[i + 1] 
        B = cdata.data[i + 2]
        A = cdata.data[i + 3]
        //console.log(`Pixn: ${ i / 4 }:-->`, R,G,B,A)
    
      if(((R > rmin) && (R < rmax )) && ((G > gmin) && (G < gmax )) && ((B > bmin) && (B < bmax ))){// condicion para verificar cada pixel
            bueno++
      } else {
             malo++  // Matriz que pinta de color rojo sino se cumple la condicion anterior 
          cdata.data[i + 0] = 255
          cdata.data[i + 1] = 0
          cdata.data[i + 2] = 0
          cdata.data[i + 3] = 255
      }//End Else
    }// End For
   
    contextcanvasx.putImageData(cdata,0,0)// Dibuja los pixeles rojos encontrados 
    
    await limpiaR(point, canvasx) // Se manda llamar limpiar ruido 

    let pixitotal=(cdata.data.length)/4

    /*/console.log( "Buenos: "+ bueno)
    console.log("******")
    console.log("Malo: "+ malo)
    console.log("Ruido:"+ruido)
    console.log("Finetuning:"+latapona)
    console.log("******")*/
    //console.log("Pix Total:"+pixitotal)

    let pixibuenos= bueno + ruido + latapona
    let porcentajebueno= pixibuenos/pixitotal
    
    porcentajefinal = porcentajebueno.toFixed(4);
    if(porcentajefinal > .995){porcentajefinal = 1}  //valor final de pase al 99.6%
    porcentajeArray[point]= porcentajefinal
    mtxw[point]= porcentajefinal

    //porcentajeArray[point]= porcentajefinal
    //console.log("Pix Total:"+pixitotal)
    //console.log(pixibuenos)
    
    if(porcentajebueno > criterio ) { statusx = "1"}else{ statusx= "0"}
    console.log("TA-"+point+":"+porcentajefinal*100+"(%)")//+" Status: "+status
    //console.log(status)
    resolve('resolved')})
}//End analiza

//**************************************************************************funciones de la camara */
function open_cam(point){// Resolve de 2 segundos

	return new Promise(async resolve =>{
    let camid
    /*
    if(point == 1) {camid="6bbc03e9010ca348936ba3691bf214301a5106305eaff95a573c305f9edc21fc"}//1 ID de camara en estacion
    if(point == 2) {camid="436349e3d8d1b6738e04f20418bb4d07e2c7aeaa8ca7ff53c336c1ba76896574"}//2
    if(point == 3) {camid="7104e08d05ca38c42991fade9818169d5a038d70971cec4e5ae452f20c977dc6"}//3
    if(point == 4) {camid="db76624b30de6e85cb6e4e960d9f1785936a6a28ffd3249e7226e810084941e3"}//4 
    if(point == 5) {camid="1781a5b51b5c9942a2d31b378a7836b4d56ee0a3f6a7e3f51a3cbf49e16c46cd"}//5
    if(point == 6) {camid="1511fbb2f4759d77159baac90d59a0958a30c02b3be651ebf99159e974d13840"}//6
    if(point == 7) {camid="d3d01162a63c5869ce3b3337d2233e767387908ad7efe9efe6e4346f3177d697"}//7 
    */
    if(point == 7) {camid="c88d919cc025aee487f1611cdedbdb76c1af4ba5933751126e01daff841ee19d"}//1 ID de camara en estacion
    if(point == 1) {camid="19b1bc5dfb1962122326a7d5c4ce9989706491380e044aac700278e6ceb42967"}//2
    if(point == 6) {camid="7777594177297550d938b1be90f7894ab2e7583b55326b7a6ff9546c2d999f6a"}//3
    if(point == 2) {camid="bd5da7ba6db50f823217c781cd7ad61687922da3940665776171e07a97009104"}//4 
    if(point == 3) {camid="a0567e9cff67a934b5dbadde771b0b1910e8adf64994efbf83cc85ae63c7904d"}//5
    if(point == 5) {camid="6e0c6d898a484e05155959e38ad799635b3883cba5aa012eebaa54b3aa82988a"}//6
    if(point == 4) {camid="73a36572238b5801a1ae1641b46f5efcd77637789232507c460e67cc417d2a22"}//7 

    const video = document.querySelector('video');
     const vgaConstraints = {
            video: {deviceId: camid} //width: {exact: 280}, height: {exact: 280} / deviceId: "5bba2c7c9238e1d8ab5e90e2f2f94aa226749826319f6c705c5bfb5a3d2d5279"
        };
            navigator.mediaDevices.getUserMedia(vgaConstraints).
            then((stream) => {video.srcObject = stream});

            setTimeout(function fire(){resolve('resolved');},2500);
        });//Cierra Promise principal
}

function captureimage(){// Resolve de 2 segundos
        return new Promise(async resolve =>{
           
            let image = document.getElementById( 'canvas2' );
            let contexim2 = image.getContext( '2d' );		
                
            var video = document.getElementById("videoElement");
            
            w = image.width;
            h= image.height;
            
            contexim2.drawImage(video,0,0,image.width,image.height);
            //var dataURI = canvas.toDataURL('image/jpeg');
        	setTimeout(function fire(){resolve('resolved');},2000);//Temporal para programacion de secuencia
        	});

}

function mapcams(){
    navigator.mediaDevices.enumerateDevices()
    .then(devices => {
    const filtered = devices.filter(device => device.kind === 'videoinput');			
                         console.log('Cameras found',filtered);
                          });
}

function stopcam(){
    return new Promise(async resolve =>{

        const video = document.querySelector('video');
        // A video's MediaStream object is available through its srcObject attribute
        const mediaStream = video.srcObject;
        // Through the MediaStream, you can get the MediaStreamTracks with getTracks():
        const tracks = mediaStream.getTracks();
        //console.log(tracks);
        // Tracks are returned as an array, so if you know you only have one, you can stop it with: 
        //tracks[0].stop();
        // Or stop all like so:
        tracks.forEach(track => {track.stop()})//;console.log(track);

    setTimeout(function fire(){resolve('resolved');},1000);
    });//Cierra Promise principal
}

function snapshot(){//Back end sent URI,SN? & point?
	return new Promise(async resolve =>{
		//let image1 = document.getElementById( 'fullimage' );
		//let contexim1 = image1.getContext( '2d' );		
			
		//var video = document.getElementById("webcam_conveyor");
		
		//w = image1.width;
		//h= image1.height;
		
		//contextfullimage.drawImage(fullimage,0,0,fullimage.width,fullimage.height);
		var dataURI = fullimage.toDataURL('image/jpeg');
		savepic(dataURI,snfile,point); //savepic(dataURI,point);
		//console.log("Pic Sent--"+sn+"--"+point);
		setTimeout(function fire(){resolve('resolved');},2000);//Temporal para programacion de secuencia
		});
		}	

function savepic(uri,snr,point){
           // let serialnumber="Unit_under_test" 
            //let point = 2           
            const socket = io();
            socket.emit('picsaving',uri,snr,point);	
        }

function logsaving(snr,logdata){
    //return new Promise(async resolve =>{

    socket.emit('logsaving',snr,logdata);

   // setTimeout(function fire(){resolve('resolved');},2000);//Temporal para programacion de secuencia
   // });
}
//********************** Funciones para dibujar recuadros de estatus
function pointstatus(cuadrante, statusx){ 
   

    if((cuadrante == 1)&&(statusx == '1')) {cuadroVerde1();} 
    if((cuadrante == 2)&&(statusx == '1')) {cuadroVerde2();}
    if((cuadrante == 3)&&(statusx == '1')) {cuadroVerde3();}
    if((cuadrante == 6)&&(statusx == '1')) {cuadroVerde4();}
    if((cuadrante == 5)&&(statusx == '1')) {cuadroVerde5();}
    if((cuadrante == 4)&&(statusx == '1')) {cuadroVerde6();}
    if((cuadrante == 7)&&(statusx == '1')) {cuadroVerde7();}
    if((cuadrante == 1)&&(statusx == '0')) {cuadroRojo1();}
    if((cuadrante == 2)&&(statusx == '0')) {cuadroRojo2();}
    if((cuadrante == 3)&&(statusx == '0')) {cuadroRojo3();}
    if((cuadrante == 6)&&(statusx == '0')) {cuadroRojo4();}
    if((cuadrante == 5)&&(statusx == '0')) {cuadroRojo5();}
    if((cuadrante == 4)&&(statusx == '0')) {cuadroRojo6();}
    if((cuadrante == 7)&&(statusx == '0')) {cuadroRojo7();}
}
function cuadroVerde1(){ 
    let canvasjs = document.getElementById('canvas')
    let context = canvasjs.getContext('2d')
   
    context.strokeStyle = "#76FF03";
    context.lineWidth = 3;
    context.strokeRect(355, 72, 158, 80); // Coordenadas del dibujo que se va a pintar en el canvas 
}
function cuadroVerde2(){ 
    let canvasjs = document.getElementById('canvas')
    let context = canvasjs.getContext('2d')
    
    context.strokeStyle = "#76FF03";
    context.lineWidth = 3;
    context.strokeRect(356, 157, 158, 75);

}
function cuadroVerde3(){ 
    let canvasjs = document.getElementById('canvas')
    let context = canvasjs.getContext('2d')
   
    context.strokeStyle = "#76FF03";
    context.lineWidth = 3;
    context.strokeRect(357, 237, 158, 81);

}
function cuadroVerde4(){ 
    let canvasjs = document.getElementById('canvas')
    let context = canvasjs.getContext('2d')
   
context.strokeStyle = "#76FF03";
context.lineWidth = 3;
context.strokeRect(585, 244, 79, 71);

}
function cuadroVerde5(){ 
    let canvasjs = document.getElementById('canvas')
    let context = canvasjs.getContext('2d')
   
context.strokeStyle = "#76FF03";
context.lineWidth = 3;
context.strokeRect(582, 154, 162, 78);

}
function cuadroVerde6(){ 
    let canvasjs = document.getElementById('canvas')
    let context = canvasjs.getContext('2d')
    
context.strokeStyle = "#76FF03";
context.lineWidth = 3;
context.strokeRect(586, 72, 78, 71);

}
function cuadroVerde7(){ 
    let canvasjs = document.getElementById('canvas')
    let context = canvasjs.getContext('2d')
   
context.strokeStyle = "#76FF03";
context.lineWidth = 3;
context.strokeRect(727, 57, 85, 115);

}
function cuadroRojo1(){  
    let canvasjs = document.getElementById('canvas')
    let context = canvasjs.getContext('2d')
    
    context.strokeStyle = "#FF0000"; // color de figura dentro de un canvas
    context.lineWidth = 3; // tamaño de grosor de la figura
    context.strokeRect(355, 72, 158, 80); // coordenadas de imagen
}
function cuadroRojo2(){  
    let canvasjs = document.getElementById('canvas')
    let context = canvasjs.getContext('2d')
    
    context.strokeStyle = "#FF0000";
    context.lineWidth = 3;
    context.strokeRect(356, 157, 158, 75);
}
function cuadroRojo3(){  
    let canvasjs = document.getElementById('canvas')
    let context = canvasjs.getContext('2d')
    
    context.strokeStyle = "#FF0000";
    context.lineWidth = 3;
    context.strokeRect(357, 237, 158, 81);
}
function cuadroRojo4(){  
    let canvasjs = document.getElementById('canvas')
    let context = canvasjs.getContext('2d')
   
    context.strokeStyle = "#FF0000";
    context.lineWidth = 3;
    context.strokeRect(585, 244, 79, 71);
}
function cuadroRojo5(){ 
    let canvasjs = document.getElementById('canvas')
    let context = canvasjs.getContext('2d')
    
    context.strokeStyle = "#FF0000";
    context.lineWidth = 3;
    context.strokeRect(582, 154, 162, 78);
}
function cuadroRojo6(){  
    let canvasjs = document.getElementById('canvas')
    let context = canvasjs.getContext('2d')
    
    context.strokeStyle = "#FF0000";
    context.lineWidth = 3;
    context.strokeRect(586, 72, 78, 71);
}
function cuadroRojo7(){  
    let canvasjs = document.getElementById('canvas')
    let context = canvasjs.getContext('2d')
   
    context.strokeStyle = "#FF0000";
    context.lineWidth = 3;
    context.strokeRect(727, 57, 85, 115);
}

//********************************************************************funciones de fin de secuencia */
function logresult(pointemp,statusl){// Guarda valor de cada punto analizado
    
    logsave[pointemp]= statusl//"TA"+pointemp+","+statusl+"&";
   // logsave[1,2,3,4,5,6,7] // puntos de imagen a guardar 
   // console.log(logsave)  ------TA1,Pass&TA2,fail&
   // console.log("Esto guardo en la posicion "+point+"Valor"+logsave[point])

}

function evalpf(){ //Evalua la matriz de logsave    
    let valtus 
    return new Promise(async resolve =>{   
//primer cuadrante
        cuadranteArray[0]= logsave[1]
        cuadranteArray[1]= logsave[2]
        cuadranteArray[2]= logsave[3]
        cuadranteArray[3]= logsave[4]
        cuadranteArray[4]= logsave[5]
        valtus = cuadranteArray.some((e) => e == "0") // funcion para buscar dentro de un array solo un valor 
        //console.log("Valtus"+valtus)
        if(valtus == true){ valtus = "0"}else{ valtus = "1"}
        //if(valtus == false){ valtus = "1" ; console.log("Dentro de if 1") }
        pointstatus(1, valtus)
        //console.log("DespuesV:"+valtus)
        
// segundo cuadrante 
        cuadranteArray[0]= logsave[6]
        cuadranteArray[1]= logsave[7]
        cuadranteArray[2]= logsave[8]
        cuadranteArray[3]= logsave[9]
        cuadranteArray[4]= logsave[10]
        valtus = cuadranteArray.some((e) => e == "0")
        if(valtus == true){ valtus = "0"}else{ valtus = "1"}
        //if(valtus == true){ valtus = "0" }
        //if(valtus == false){ valtus = "1" }
        pointstatus(2, valtus)

// tercer cuadrante 
        cuadranteArray[0]= logsave[11]
        cuadranteArray[1]= logsave[12]
        cuadranteArray[2]= logsave[13]
        cuadranteArray[3]= logsave[14]
        cuadranteArray[4]= logsave[15]
        valtus = cuadranteArray.some((e) => e == "0")
        if(valtus == true){ valtus = "0"}else{ valtus = "1"}
        pointstatus(3, valtus)
        cuadranteArray=[] //Limpia matriz de trabajo
// cuarto cuadrante 
        cuadranteArray[0]= logsave[16]
        cuadranteArray[1]= logsave[17]    
        valtus = cuadranteArray.some((e) => e == "0")
        if(valtus == true){ valtus = "0"}else{ valtus = "1"}
        pointstatus(4, valtus)

// quinto cuadrante 
        cuadranteArray[0]= logsave[18]
        cuadranteArray[1]= logsave[19]
        cuadranteArray[2]= logsave[20]
        cuadranteArray[3]= logsave[21]
        cuadranteArray[4]= logsave[22]
        valtus = cuadranteArray.some((e) => e == "0")
        if(valtus == true){ valtus = "0"}else{ valtus = "1"}
        pointstatus(5, valtus)
        cuadranteArray=[] //Limpia matriz de trabajo
// sexto cuadrante 
        cuadranteArray[0]= logsave[23]
        cuadranteArray[1]= logsave[24]    
        valtus = cuadranteArray.some((e) => e == "0")
        if(valtus == true){ valtus = "0"}else{ valtus = "1"}
        pointstatus(6, valtus)

// septimo cuadrante 
        cuadranteArray[0]= logsave[25]
        cuadranteArray[1]= logsave[26]    
        valtus = cuadranteArray.some((e) => e == "0")
        if(valtus == true){ valtus = "0"}else{ valtus = "1"}
        pointstatus(7, valtus)
//******* */
  let resultadofinal =  logsave.some((e) => e == "0")
  if(resultadofinal==false){console.log("Unit---> Pass")}else{console.log("Unit---> Fail")}
  
  resolve('resolved')}) //end promise 
} 


//*******************************************************************funciones de debug */
let calis = new Image()// Variable utilizada por switchpic

function canbugshow(){
    return new Promise(async resolve =>{
    document.getElementById( 'canvica' ).style.visibility = "visible"
    document.getElementById( 'canvande').style.visibility = "visible"
    document.getElementById( 'canvedio' ).style.visibility = "visible"
    document.getElementById( 'canvediojr' ).style.visibility = "visible"
    document.getElementById( 'canhorizontal' ).style.visibility = "visible"
    document.getElementById( 'canparalelo' ).style.visibility = "visible"
    document.getElementById( 'canvach' ).style.visibility = "visible"
    document.getElementById( 'canvage' ).style.visibility = "visible"
    document.getElementById( 'tb1' ).style.visibility = "hidden"
    document.getElementById( 'tb2' ).style.visibility = "hidden"
    document.getElementById( 'tb3' ).style.visibility = "hidden"
    document.getElementById( 'tb4' ).style.visibility = "hidden"
    setTimeout(function fire(){resolve('resolved');},1000);})
}

function canbughide(){

document.getElementById( 'canvica' ).style.visibility = "hidden"
document.getElementById( 'canvande').style.visibility = "hidden"
document.getElementById( 'canvedio' ).style.visibility = "hidden"
document.getElementById( 'canvediojr' ).style.visibility = "hidden"
document.getElementById( 'canhorizontal' ).style.visibility = "hidden"
document.getElementById( 'canparalelo' ).style.visibility = "hidden"
document.getElementById( 'canvach' ).style.visibility = "hidden"
document.getElementById( 'canvage' ).style.visibility = "hidden"
document.getElementById( 'tb1' ).style.visibility = "hidden"
document.getElementById( 'tb2' ).style.visibility = "hidden"
document.getElementById( 'tb3' ).style.visibility = "hidden"
document.getElementById( 'tb4' ).style.visibility = "hidden"
}

//C:\Users\mayra_ayala\Documents\Aquiles\img\tim_module
//C:\Users\mayra_ayala\Documents\Aquiles\img\tim_module\New
function switchpic(name){
    calis.src = "/img/tim_module/New/"+name+".jpg"
}

function loadcalis(fotox){//Funcion Carga la imagen del modelo 
   // return new Promise(async resolve =>{
    switchpic(fotox)
   // console.log(calis.src)
   setTimeout(function dibuja(){
       contextfullimage.drawImage(calis, 0, 0, calis.width, calis.height, 0, 0, contextfullimage.canvas.width, contextfullimage.canvas.height)
    },300)
       //setTimeout(function fire(){resolve('resolved');},1000);});//Cierra Promise principal
}

function cuentarojos(canvasx,x,y,w,h){ //cuenta puntos rojos de la coordenada con el tamaño especificado

    let contextcanvasx = canvasx.getContext( '2d' )
    let cdata = contextcanvasx.getImageData(x,y,w,h);
    let rojos=0
    let otros=0

    contextcanvasx.strokeStyle="#3333ff"
    contextcanvasx.lineWidth = 1
    contextcanvasx.strokeRect(x,y,w,h)

    for (let i = 0; i <cdata.data.length; i += 4) { //cdata.data.length
        // Matriz para valores 
            R = cdata.data[i + 0]
        if(R == 255){rojos++}
            else {otros++}
        }// end for
    console.log("Red :"+rojos)
    console.log("Others :"+otros)
}


//****************************************************************funciones de alineamiento

//Fiducial work variables
let searchstartx,searchstarty,searchendx,searchendy
let pointAx,pointAy,pointBx,pointBy
 async function fiducialselect(point){
    //Search areas quadrant 
    pointAx=0 // Limpia coordenadas finales para el punto 1 en x
    pointAy=0 // Limpia coordenadas finales para el punto 1 en y   
    if (point==1){
        loadfiducial("C1A");searchstartx=1547;searchstarty=188;searchendx=1786;searchendy=388;
        startxareasearch=245,startyareasearch=1655
        endxareasearch=264,endyareasearch=1674
        await sampler(fullimage,fiducial)
        pointAx=cordx
        pointAy=cordy
       //loadfiducial("C1B");searchstartx=1547;searchstarty=188;searchendx=1786;searchendy=388;
        console.log("X1,Y1: "+"("+pointAx+","+pointAy+")")

    }
    if (point==2){
        loadfiducial("c2")
    }

}

function rotatecuadrant(canvasx,angle){
    let contextcanvasx = canvasx.getContext( '2d' )
    contextcanvasx.rotate(angle * Math.PI / 180);
}

function switchfid(cuadrante){
    fidupic.src = "/img/tim_module/fiducials/"+cuadrante+".jpg"
}

function loadfiducial(fidpic){//Funcion Carga fiducial
    // return new Promise(async resolve =>{
    switchfid(fidpic)
    //setTimeout(function dibujafiducial(){
    contextfiducial.drawImage(fidupic, 0, 0, fidupic.width, fidupic.height, 0, 0, contextfiducial.canvas.width, contextfiducial.canvas.height)
    // },200)
        //setTimeout(function fire(){resolve('resolved');},1000);});//Cierra Promise principal
}

//***************************************** */
// Variables para definir area de sampleo de imagen  (La funcion sampler utiliza estas variables)
let startxareasearch=0 // Donde empieza a buscar en fullimage en y (viceversa =( 
let startyareasearch=0 // Donde empieza a buscar en fullimage en x (viceversa =(
let endxareasearch=0 // Donde Termina de buscar en fullimage en y (viceversa =(
let endyareasearch=0 // Donde Termina de buscar en fullimage en x (viceversa =(

// Variables para definir el cuadro de seccion a recortar de la imagen completa "fits(Full image tensor section coordenadas)" (La funcion sampler utiliza estas variables)
let xfits=0
let yfits=0
let xfitsend=20
let yfitsend=20
imgpixsize=1200 //20x20x3
//Variables para contador de ciclos For utilizadso por la funcion sampler()
//Area de busqueda 
let rowcount=0 ;
let colcount=0 ;
let cordx=0,cordy=0; //Coordenadas finales
//Variables de memoria utilizadas en la funcion locate()
let wbmatch,wmtxf
wmtxf=50; //Work matrix factor(Factor M)
wbmatch=100;//Work best match

function sampler(fullimage,imagesample){ 

    //console.table( tf.memory() ) Ayuda a ver la tabla de memoria utilizada por tf        
    return new Promise(async resolve =>{
    tf.tidy(() => {// Engolba todos los vectores y memoria usada por tf para limpiar al final
	let sampletensor=tf.browser.fromPixels(imagesample) //Sample image a tensor
	let fullimagetensor=tf.browser.fromPixels(fullimage) // Fullimage a tensor
	//let fullimagetensorsection=tf.slice(fullimagetensor,[yfits,xfits], [xfitsend,yfitsend])//Seccion recortada de fullimage , fits(Full image tensor section coordenadas)


	//Encuentra punto de alineacion
    
	for (rowcount=0 + startxareasearch; rowcount < endxareasearch ; rowcount++){
		
		for (colcount = 0 + startyareasearch; colcount < endyareasearch; colcount++){
		//Toma muestras
			let slicematrix1=tf.slice(fullimagetensor,[rowcount,colcount], [xfitsend,yfitsend]); //slice(entrada,[Renglon(height),Columna(width)] "inicio",[Renglon final,columna final] Tamaño de slice
				
		//Valores para imprimir en pantalla 			 
			let maxmatrix = slicematrix1.div(sampletensor).max(); //Divide y guarda el valor maximo dentro de la matriz (Regresa tf.tensor)
			let sum = slicematrix1.div(sampletensor).sum(); //Suma de elementos de la division slicematrix2 y slicematrix1
			
			//tf.print(maxmatrix)
			//tf.print(sum)
		//Valores para calculo de match
			let sampleval=tf.scalar(imgpixsize);
			let percen=tf.scalar(100); //100%
			let match = tf.div(sum,sampleval).mul(percen).round();//% match between suma de divicion entre slicematrix2 y slicematrix1
			//Transforma match tensor a minibuffer para sacar valores en simples numeros
			let matchminibuff=tf.buffer(match.shape,match.dtype,match.dataSync());
			//Transforma maxmatrix tensor a minibuffer para sacar valores en simples numeros
			let maxmatrixminibuff=tf.buffer(maxmatrix.shape,maxmatrix.dtype,maxmatrix.dataSync());
			
		//Obtiene la mejor coordenada
			
			locate(matchminibuff.get(0),maxmatrixminibuff.get(0),rowcount,colcount);	 							                                                   
		   // console.log(matchminibuff.get(0)) //console.log("Match",matchminibuff.print)
			//console.log("Max",maxmatrixminibuff.print)
            
			} // Fin ciclo for col
            
	} //Fin ciclo for row
   
	//console.log("Coordenadas elegidas: X->",cordx,"Y->",cordy) 
    });//tf.tidy end   
    resolve('resolved')})
	//pinanalisis(cordx,cordy)
	//rectangle(cordy,cordx)	
   
}

function locate(bmatch,mtxf,rowc,colc){ //factorm,bestmatch,cordx,cordy
	
	tf.tidy(() => {// Engolba todos los vectores y memoria usada por tf para limpiar al final
	//console.log("Valores max:"+ mtxf+" Match: "+bmatch+" Y: "+colc+" X: "+rowc);
	let tmpmatch=0, tmpwmtxf=0; //Valores temporales
	
	
	if (mtxf <= wmtxf){
		
		tmpwmtxf = mtxf;//El valor mas bajo del factor M	
		tmpmatch=bmatch;//El valor temporal actual de match
		
			if (tmpmatch < 101 && tmpmatch > 0){
				if(tmpwmtxf < wmtxf){
					wbmatch=tmpmatch;
					wmtxf = mtxf;
					//Asigna cordenadas de alineacion
					cordx=rowc;
					cordy=colc;}
				else
					if (tmpmatch > wbmatch){
						wbmatch=tmpmatch; 
						wmtxf = mtxf;
						//Nuevo renglon
						cordx=rowc;
						cordy=colc;}
                    /*
					console.log ("<------------------>");
					console.log("tmpmatch: ",tmpmatch);
					console.log("wbmatch: ",wbmatch);						
					console.log("wmtxf: ",wmtxf);
					console.log("rowc: ",rowc);
					console.log("colc: ",colc);
					console.log("selectedrowc: ",cordx);
					console.log("selectedcolc: ",cordy);
					console.log ("<------------------>");
					*/

					let imagematrix=tf.browser.fromPixels(fullimage);		
					sliceresult=tf.slice(imagematrix,[cordx,cordy], [xfitsend,yfitsend]);
					//tf.browser.toPixels (sliceresult,tensorimage); 
					/*
					console.log ("<******************>");
					console.log("selectedrowc: ",cordx);
					console.log("selectedcolc: ",cordy);
					console.log ("<******************>");
                    */
		    }
							
	    }
    });//tf.tidy end  
}

function limpiaR(p,canvasx){
    return new Promise(async resolve =>{
    let contextcanvasx = canvasx.getContext( '2d' )

    let azul = 0
    let otros = 0
    let x = 0
    let y = 0
    let w = 0
    let h = 0

    //Cuadrante 1 
    if(p == 1){x = 95, y = 0, w = 200,h = 100} //TA1  Coordenadas de rectangulos donde pinta azules 
    if(p == 2){x = 385, y = 28, w = 153,h = 143} //TA2
    if(p == 11){x = 433, y = 41, w = 106,h = 111} //TA11
    if(p == 12){x = 1, y = 35, w = 173,h = 120} //TA12
    if(p == 23){x = 6, y = 7, w = 64,h = 63} //TB1
    //Cuadrante 2
    if(p == 3){x = 2, y = 0, w = 143, h = 78} // TA3
    if(p == 4){x = 252, y = 12, w = 116,h = 107} //TA4
    if(p == 9){x = 7, y = 77, w = 357, h = 40} //TA9
    if(p == 10){x = 12, y = 9, w = 345, h = 112} //TA10
    if(p == 24){x = 5, y = 4, w = 42, h = 43} //TB2
    //Cuadrante 3
    if(p == 5){x = 275, y = 80, w = 203, h = 883} //TA5
    if(p == 6){x = 1, y = 23, w = 199, h = 144} //TA6
    if(p == 7){x = 0, y = 30, w = 159, h = 137} //TA7
    if(p == 8){x = 395, y = 37, w = 144, h = 124} //TA8 
    if(p == 25){x = 7, y = 5, w = 63, h = 65} // TB3
    //Cuadrante4
    if(p == 17){x = 53, y = 19, w = 393, h = 160} //TA17
    if(p == 18){x = 40, y = 23, w = 416, h = 138} //TA18
    //Cuadrante 5
    if(p == 15){x = 28, y = 10, w = 352, h = 135} //TA15
    if(p == 16){x = 34, y = 12, w = 341, h = 197} // TA16 
    if(p == 19){x = 17, y = 0, w = 124, h = 382} //TA19
    if(p == 20){x = 18, y = 211,w = 122, h = 239} //TA20
    if(p == 26){x = 6, y = 5, w = 52, h = 54} // TB4
    //Cuadrante 6 
    if(p == 13){x = 65,y = 25,w = 373,h = 141} //TA13
    if(p == 14){x = 60, y = 9, w = 366, h = 150} //TA14
    //Cuadrante 7 
    if(p == 21){x = 0, y = 6, w = 25, h = 450} //TA21
    if(p == 22){x = 91, y = 1, w = 165, h = 650} //TA22

    let cdata = contextcanvasx.getImageData(x,y,w,h) // linea para pintar 

    for (let i = 0; i <cdata.data.length; i += 4){ // ciclo de matriz /camina la matriz las veces que tiene el ciclo
        // matriz para pintar color azul si encuentra un rojo
        R = cdata.data[i + 0]
        if(R == 255){
            cdata.data[i + 0] = 0
            cdata.data[i + 1] = 0
            cdata.data[i + 2] = 255
            cdata.data[i + 3] = 255
        B = cdata.data[i + 2]
        if(B == 255){azul++}
        //else {otros++}
    }
}// end for
    //console.log(cdata.data.length)
    //console.log("Blue :"+azul)
    ruido = azul 
    contextcanvasx.putImageData(cdata,x,y)// Dibuja los pixeles rojos encontrados
    contextcanvasx.strokeStyle="#0000FF"
    contextcanvasx.lineWidth = 1
    contextcanvasx.strokeRect(x,y,w,h)
    resolve('resolved')})
}

async function split(infoplc){ // S&IDM-2007&P1093219-00-G:SBNJ19194020602&LFTM1135558-04-A&START#



station = infoplc.toString().substr(2,8); console.log(station)
sn = infoplc.toString().substr(11,29); console.log(sn)
pn = infoplc.toString().substr(41,16); console.log(pn)
    await serialnumber (sn)
    await partnumber (pn)
    await st(station)

}


 function ita(po){
    return new Promise(async resolve =>{
        //await loadcalis(point)
           /*await recorTA(po)
           await analiza(po)
           await canbugshow(po)*/
           // cuadrante 1 
        if(po == 1){ contextcanvica.drawImage(fullimage,240,342,486,192,0,0,contextcanvica.canvas.width,contextcanvica.canvas.height); // coordenada y tamaño de recorte en el canvas }
            contextfullimage.strokeStyle="#ffff00"
            contextfullimage.lineWidth = 2
            contextfullimage.strokeRect(240,342,486,192)//Dibuja rectangulo en canvas2
            await analiza(canvica,1)}
        if(po == 2){contextcanvande.drawImage(fullimage,232,591,538,192,0,0,contextcanvande.canvas.width,contextcanvande.canvas.height);
            contextfullimage.strokeStyle="#ffff00"
            contextfullimage.lineWidth = 2
            contextfullimage.strokeRect(232,591,538,192)
            await analiza(canvande,2)}
        if(po == 11){contextcanvande.drawImage(fullimage,971,797,538,192,0,0,contextcanvande.canvas.width,contextcanvande.canvas.height);
            contextfullimage.strokeStyle="#ffff00"
            contextfullimage.lineWidth = 2
            contextfullimage.strokeRect(971,797,538,192)
            await analiza(canvande,11)}
        if(po == 12){contextcanvande.drawImage(fullimage,1075,391,538,192,0,0,contextcanvande.canvas.width,contextcanvande.canvas.height);
            contextfullimage.strokeStyle="#ffff00"
            contextfullimage.lineWidth = 2
            contextfullimage.strokeRect(1075,391,538,192)  
            await analiza(canvande,12)}
        if(po == 23){contexttb1.drawImage(fullimage,419,253,77,77,0,0,contexttb1.canvas.width,contexttb1.canvas.height);
            contextfullimage.strokeStyle="#ffff00"
            contextfullimage.lineWidth = 2
            contextfullimage.strokeRect(419,253,77,77)
            await analiza(tb1,23)}

             //Cuadrante 2 
        if(po == 3){contextcanvediojr.drawImage(fullimage,447,260,332,131,0,0,contextcanvediojr.canvas.width,contextcanvediojr.canvas.height);
            contextfullimage.strokeStyle="#ffff00"
            contextfullimage.lineWidth = 2
            contextfullimage.strokeRect(447,260,332,131)//Dibuja rectangulo en canvas2
            await analiza(canvediojr,3)}
        if(po == 4){contextcanvedio.drawImage(fullimage,413,619,367,131,0,0,contextcanvedio.canvas.width,contextcanvedio.canvas.height);
            contextfullimage.strokeStyle="#ffff00"
            contextfullimage.lineWidth = 2
            contextfullimage.strokeRect(413,619,367,131)//Dibuja rectangulo en canvas2*/
            await analiza(canvedio,4)}
        if(po == 9){contextcanvedio.drawImage(fullimage,977,576,367,131,0,0,contextcanvedio.canvas.width,contextcanvedio.canvas.height);
            contextfullimage.strokeStyle="#ffff00"
            contextfullimage.lineWidth = 2
            contextfullimage.strokeRect(977,576,367,131)
            await analiza(canvedio,9)}
        if(po == 10){contextcanvedio.drawImage(fullimage,1071,284,367,131,0,0,contextcanvedio.canvas.width,contextcanvedio.canvas.height);
            contextfullimage.strokeStyle="#ffff00"
            contextfullimage.lineWidth = 2
            contextfullimage.strokeRect(1071,284,367,131)
            await analiza(canvedio,10)}
        if(po == 24){contexttb2.drawImage(fullimage,485,198,52,52,0,0,contexttb2.canvas.width,contexttb2.canvas.height);
            contextfullimage.strokeStyle="#ffff00"
            contextfullimage.lineWidth = 2
            contextfullimage.strokeRect(485,198,52,52)
            await analiza(tb2,24)}

            //Cuadrante 3 
        if(po == 5){contextcanvica.drawImage(fullimage,1160,751,486,192,0,0,contextcanvica.canvas.width,contextcanvica.canvas.height);
            contextfullimage.strokeStyle="#ffff00"
            contextfullimage.lineWidth = 2
            contextfullimage.strokeRect(1160,751,486,192)
            await analiza(canvica,5)}        
        if(po == 6){contextcanvande.drawImage(fullimage,1139,244,538,192,0,0,contextcanvande.canvas.width,contextcanvande.canvas.height);
            contextfullimage.strokeStyle="#ffff00"
            contextfullimage.lineWidth = 2
            contextfullimage.strokeRect(1139,244,538,192)
            await analiza(canvande,6)}   
        if(po == 7){contextcanvande.drawImage(fullimage,355,317,538,192,0,0,contextcanvande.canvas.width,contextcanvande.canvas.height);
            contextfullimage.strokeStyle="#ffff00"
            contextfullimage.lineWidth = 2
            contextfullimage.strokeRect(355,317,538,192)
            await analiza(canvande,7)}
        if(po == 8){contextcanvande.drawImage(fullimage,251,716,538,192,0,0,contextcanvande.canvas.width,contextcanvande.canvas.height);
            contextfullimage.strokeStyle="#ffff00"
            contextfullimage.lineWidth = 2
            contextfullimage.strokeRect(251,716,538,192)
            await analiza(canvande,8)}
        if(po == 25){contexttb3.drawImage(fullimage,1512,966,77,77,0,0,contexttb3.canvas.width,contexttb3.canvas.height);
            contextfullimage.strokeStyle="#ffff00"
            contextfullimage.lineWidth = 2
            contextfullimage.strokeRect(1512,966,77,77)
            await analiza(tb3,25)}

            //Cuadrante 4
        if(po == 17){contextcanvica.drawImage(fullimage,626,616,486,192,0,0,contextcanvica.canvas.width,contextcanvica.canvas.height);
            contextfullimage.strokeStyle="#ffff00"
            contextfullimage.lineWidth = 2
            contextfullimage.strokeRect(626,616,486,192)
            await analiza(canvica,18)}
        if(po == 18){contextcanvica.drawImage(fullimage,559,344,486,192,0,0,contextcanvica.canvas.width,contextcanvica.canvas.height);
            contextfullimage.strokeStyle="#ffff00"
            contextfullimage.lineWidth = 2
            contextfullimage.strokeRect(559,344,486,192)
            await analiza(canvica,18)}

            //Cuadrante 5
        if(po == 15){contextcanhorizontal.drawImage(fullimage,789,434,408,161,0,0,contextcanhorizontal.canvas.width,contextcanhorizontal.canvas.height);
            contextfullimage.strokeStyle="#ffff00"
            contextfullimage.lineWidth = 2
            contextfullimage.strokeRect(789,434,408,161)
            await analiza(canhorizontal,15)}
        if(po == 16){contextcanhorizontal.drawImage(fullimage,851,659,408,161,0,0,contextcanhorizontal.canvas.width,contextcanhorizontal.canvas.height);
            contextfullimage.strokeStyle="#ffff00"
            contextfullimage.lineWidth = 2
            contextfullimage.strokeRect(851,659,408,161)
            await analiza(canhorizontal,16)}
        if(po == 19){contextcanparalelo.drawImage(fullimage,390,352,161,450,0,0,contextcanparalelo.canvas.width,contextcanparalelo.canvas.height);
            contextfullimage.strokeStyle="#ffff00"
            contextfullimage.lineWidth = 2
            contextfullimage.strokeRect(400,352,161,450)
             await analiza(canparalelo,19)}
        if(po == 20){contextcanparalelo.drawImage(fullimage,177,351,161,450,0,0,contextcanparalelo.canvas.width,contextcanparalelo.canvas.height);
            contextfullimage.strokeStyle="#ffff00"
            contextfullimage.lineWidth = 2
            contextfullimage.strokeRect(192,351,161,450)
            await analiza(canparalelo,20)}
        if(po == 26){contexttb4.drawImage(fullimage,608,448,64,64,0,0,contexttb4.canvas.width,contexttb4.canvas.height);
            contextfullimage.strokeStyle="#ffff00"
            contextfullimage.lineWidth = 2
            contextfullimage.strokeRect(608,448,64,64)
            await analiza(tb4,26)}

            //Cuadrante 6 
        if(po == 13){contextcanvica.drawImage(fullimage,978,280,486,192,0,0,contextcanvica.canvas.width,contextcanvica.canvas.height);
            contextfullimage.strokeStyle="#ffff00"
            contextfullimage.lineWidth = 2
            contextfullimage.strokeRect(978,280,486,192)
            await analiza(canvica,13)}
        if(po == 14){contextcanvica.drawImage(fullimage,1038,546,486,192,0,0,contextcanvica.canvas.width,contextcanvica.canvas.height);
            contextfullimage.strokeStyle="#ffff00"
            contextfullimage.lineWidth = 2
            contextfullimage.strokeRect(1038,546,486,192)
            await analiza(canvica,14)}

                //Cuadrante 7
        if(po == 21){contextcanvach.drawImage(fullimage,575,308,59,454,0,0,contextcanvach.canvas.width,contextcanvach.canvas.height);
            contextfullimage.strokeStyle="#ffff00"
            contextfullimage.lineWidth = 2
            contextfullimage.strokeRect(575,308,59,454)
            await analiza(canvach,21)}
        if(po == 22){contextcanvage.drawImage(fullimage,656,227,370,650,0,0,contextcanvage.canvas.width,contextcanvage.canvas.height);
            contextfullimage.strokeStyle="#ffff00"
            contextfullimage.lineWidth = 2
            contextfullimage.strokeRect(656,227,370,650)
            await analiza(canvage,22)}

        resolve('resolved')})
}

 function automation(){

 }




