//******** Test sequence
let serialnumber;
let points=10;
let point;
async function start(){

serialnumber = prompt("Por favor Ingresa el Serial..."); //Obtenemos el serial de la unidad
 await open_conveyor_cam();	

  //Inicia Ciclo de puntos
		var i;
		for (i = 1; i <= points; i++) {// Obtiene el punto a inspeccionar
		point=i;
		//Borrar temporizador que puse dentro de poen_conveyor todo se debe manejar por trigger flag
		await gantry_point(i);//Posicionar ganntry
		await timer(2);// + dos segundos de gantry_point timer
		await snapshot(); //por punto
		}
		gantry_point(0)//Back home
}
//******** Camera & Image Saving
function open_conveyor_cam(){
	return new Promise(async resolve =>{
	let meddev=navigator.mediaDevices.enumerateDevices(); 
	navigator.mediaDevices.enumerateDevices().then(gotDevices); 
	document.addEventListener('keyup', (e) => {resolve('resolved');console.log("Pressed");});//Se activa con remote flag cuando listo
	setTimeout(function fire(){resolve('resolved');},2000);//Temporal para programacion de secuencia
	});
 
	function gotDevices(devices) { 

		const filtdev = devices.filter(device => device.kind === 'videoinput');	
		//const videoSelect = document.querySelector('select#menu');
		
		// Bloque para iniciar la camara 0 por default
		const video = document.querySelector('video');
		let defaultcam={video:{deviceId:""+filtdev[0].deviceId+""}};
		navigator.mediaDevices.getUserMedia(defaultcam).
		then((stream) => {video.srcObject = stream});
		//setTimeout(remoteflag,2000);		
			}//Cierra sub function gotDevices
		
}

function snapshot(){//Back end sent URI,SN? & point?
	return new Promise(async resolve =>{
		let image1 = document.getElementById( 'canvas' );
		let contexim1 = image1.getContext( '2d' );		
			
		var video = document.getElementById("webcam_conveyor");
		
		w = image1.width;
		h= image1.height;
		
		contexim1.drawImage(video,0,0,image1.width,image1.height);
		var dataURI = canvas.toDataURL('image/jpeg');
		savepic(dataURI,point);
		console.log("Pic Sent");
		setTimeout(function fire(){resolve('resolved');},2000);//Temporal para programacion de secuencia
		});
		}	

function savepic(uri,point){
	const socket = io();
	socket.emit('picsaving',uri,serialnumber,point);	
}

//******** Gantry anime 	
let pos_xl = 1026.296875 ;
let pos_xb = 466.640625 ;
let pos_yl = 1049.625 ;
let pos_yt = 243.609375;
 
async function animate_gantry(x,y){

if ((x > 0)){
	for (i=1; i < x; i++) {

		await timer();
		document.getElementById("xaxis").style.left = (pos_xl + i)+"px"; 	
		document.getElementById("yaxis").style.left = (pos_yl + i)+"px"; 
			}//Cierra For x
			actual_pos();// Actualiza posicion 
		}
	else{
    
	for (i=1; i > x; i--) {

		await timer();
		document.getElementById("xaxis").style.left = (pos_xl + i)+"px"; 
		document.getElementById("yaxis").style.left = (pos_yl + i)+"px"; 
			}//Cierra For -x
			actual_pos();// Actualiza posicion 
		}//Cierra else

if ((y > 0)){		
	for (i=1; i < y ; i++) {
		
		await timer();	
		document.getElementById("yaxis").style.top = (pos_yt - i) +"px";		
			}//Cierra For y
			actual_pos();// Actualiza posicion actual
		}
		else{
		
	for (i=1; i > y ; i--) {

		await timer();
		document.getElementById("yaxis").style.top = (pos_yt - i) +"px"; 	
			}//Cierra For -y
			actual_pos();// Actualiza posicion actual
		}
}

async function actual_pos(){
  
	pos_xl = document.getElementById("xaxis").getBoundingClientRect();
	pos_xb = document.getElementById("xaxis").getBoundingClientRect();
	pos_yl = document.getElementById("yaxis").getBoundingClientRect();
	pos_yt = document.getElementById("yaxis").getBoundingClientRect();	
	
	//Actualiza valores
	pos_xl = pos_xl.left;
	pos_xb = pos_xb.bottom;
	pos_yl = pos_yl.left;
	pos_yt = pos_yt.top;	
	}

async function gantry_point(i){
	return new Promise(async resolve =>{
		tavis(i);
		console.log("Gantry point: "+i);//Temporal para programacion de secuencia
		//document.addEventListener('keyup', (e) => {resolve('resolved');console.log("Pressed");});//Se activa con remote flag cuando listo
		setTimeout(function fire(){resolve('resolved');},2000);//Temporal para programacion de secuencia
		
	});
	
}

//******** Trigger flags & Timer
function remoteflag(){
	let arrowup = new Event('keyup');
	arrowup.code = "ArrowUp";
	document.dispatchEvent(arrowup);
	console.log("Remote flag exdcuted..");
}

async function timer(t){
	return new Promise(async resolve =>{
		let waittimer= t*1000;
		setTimeout(function fire(){resolve('resolved');console.log("Timer excuted after:"+waittimer+" miliseconds.");},waittimer);
		});
}

//******** Backend call functions

function tavis(p){
	const socket = io();		
	socket.emit('tavis',p);		
}
